import wave
import scipy.io.wavfile as sc

def input_file(self, wave_file_string):

        def every_other(v, offset=0):
            return [v[i] for i in range(offset, len(v), 2)]

        """Extracts out header of WAV file
        """
        self.wave_file_string = wave_file_string

        # open file
        try:
            self.fileIn = wave.open(wave_file_string, 'r')
            print self.fileIn.__class__
        except IOError, err:
            print("Could not open file %s" % wave_file_string)
            return "fail"

        return self.fileIn

def output_file(self, wave_file_string, sample_rate, data):
    try:
        sc.write(wave_file_string[:-4] + " edited.wav", sample_rate, data)
        return True
    except:
        return False