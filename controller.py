# header.py
# Extract header info from wav file
import math
import spectral_analysis
import audio_handler
import struct
import wave
from numpy import array
import numpy
import waveio
import pdb
import noise_simulator
import scipy.io.wavfile as sc

class Controller():
    def __init__(self):
        pass

    def play_original_audio(self):
        audio_handler.play(self.wave_file_string)

    def play_noisy_audio(self):
        audio_handler.play(self.wave_file_string[:-4] + " edited.wav")

    def draw_original_graph(self):
        spectral_analysis.plot(self.data.tolist())

    def draw_noisy_graph(self):        
        spectral_analysis.plot(self.new_data.tolist())

    def generate_noise(self, snr):
        self.new_data = noise_simulator.generate_noise(self, snr, self.data)
        waveio.output_file(self, self.wave_file_string, self.sample_rate, self.new_data)
        pdb.set_trace()

    def compute_PSD(self):
        spectral_analysis.spec_draw(self.new_data, "noisy data")

    def compute_clean_PSD(self):
        spectral_analysis.spec_draw(self.data, "clean data")

    @staticmethod
    def compute_enhanced_PSD(self):
        spectral_analysis.get_noise_estimate()

    def get_wave_header(self, wave_file_string):
        self.wave_file_string = wave_file_string

        fileIn = waveio.input_file(self, wave_file_string)

        (num_channels, sample_width, self.sample_rate, num_samples, comp_type, comp_name) = fileIn.getparams()
        self.samples = fileIn.readframes(num_samples * num_channels)
        self.sample_rate, self.data = sc.read(wave_file_string)
        
