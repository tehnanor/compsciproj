import math
import numpy
from numpy.random import normal

def generate_noise(self, snr, data):
    try:
        new_data = numpy.copy(data)

        power_signal = 0
        power_noise = 0

        for item in range(0, len(data)):
            power_signal += float(data[item]) * float(data[item])
        power_signal /= len(data)

        power_noise = float(power_signal) / math.pow(10, float(snr)/10)

        power_noise = math.sqrt(power_noise)

        noise = normal(0, power_noise, len(data))

        for item in range(0, len(data)):
            new_data[item] = data[item] + noise[item]

        return new_data
    except:
        return False