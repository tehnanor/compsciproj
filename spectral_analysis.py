import matplotlib.pyplot as plt
from matplotlib.mlab import specgram
from pylab import *
from numpy.fft import rfft
import pdb
import numpy as np
import scipy.io.wavfile as sc
import data

data_things = data.Data

def plot(points):
    plt.plot(points)
    plt.show()


def complex_plot(imag_array):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(imag_array.real, imag_array.imag)
    plt.show()

def spec_draw(imag_array, title):
    try:
        temp = []
        values = []

        plt.figure()

        for x in range(0, len(imag_array) - 256, 128):
            for i in range(0, 256):
                temp.append(imag_array[x+i])

            values.append(psd(temp, sides='onesided'))
            temp = []

        for i in range(0, len(values)):
            temp.append(values[i][0])


        if title == "noisy data":
            data_things.noisy_data = np.copy(temp)
        
        plt.title(title)
        plt.plot(temp)
        plt.show()
        return True
    except:
        return False


def get_noise_estimate():

    #First 20 parts of array
    a = array(data_things.noisy_data[:20])
    #Last 20 parts of array
    b = array(data_things.noisy_data[-20:])

    #Combine the arrays
    temp = np.vstack([a, b])

    #Get the mean of the noise estimate
    a = temp.mean(axis = 0)

    spectral_subtraction(a, data_things.noisy_data)


def spectral_subtraction(noise_estimate, noisy_data):
    try:
        result = np.subtract(noisy_data, noise_estimate)
        result = result.clip(0)
        result = result.tolist()
        
        #pdb.set_trace()
        plt.figure()
        plt.suptitle("Enhanced Data")
        plt.plot(result)
        plt.show()
        return True
    except:
        return False