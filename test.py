import unittest
import waveio
import spectral_analysis
import noise_simulator as ns
from Tkinter import *
import wave
import numpy

class testWaveIO(unittest.TestCase):

    def test_wavein(self):
        fileIn = waveio.input_file(self, "this is not a file")
        self.assertEqual(fileIn, "fail")
        fileIn = waveio.input_file(self, "/home/rony/PycharmProjects/CSProject/WaveFiles/foo.wav")
        self.assertIsInstance(fileIn, wave.Wave_read)

    def test_waveout(self):
        self.assertFalse(waveio.output_file(self, "this will fail", 
            "this should be an int", "this should be an array"))
        self.assertTrue(waveio.output_file(self, "this will succeed", 
            16000, numpy.zeros(256)))

class test_noise_simulator(unittest.TestCase):
    def test_generate_noise(self):
        noise = ns.generate_noise(self, -5, numpy.zeros(256))
        self.assertFalse(noise)
        noise = ns.generate_noise(self, -5, numpy.ones(256))
        self.assertIsInstance(noise, numpy.ndarray)

class test_spectral_analysis(unittest.TestCase):
    def test_spectral_subtraction(self):
        value = spectral_analysis.spectral_subtraction("this will", "fail")
        self.assertFalse(value)

if __name__ == '__main__':
    unittest.main()