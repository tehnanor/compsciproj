#!/usr/bin/python
# -*- coding: utf-8 -*-t
#rony


from Tkinter import *
import ttk
import tkFileDialog
import controller
import data
import pdb
import unittest

root = Tk()
content = ttk.Frame(root)
controller = controller.Controller()
data = data.Data()


class Main(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent

        """Label Frames"""
        self.graph_label = LabelFrame(root, text="Graph Tools", width=300, height=300)
        self.audio_label = LabelFrame(root, text="Audio Tools", width=300, height=300)

        """Buttons"""
        self.draw_graph_button = Button(self.graph_label, text="Draw original Graph", command=self.draw_original_graph, state='disabled')
        self.draw_noisy_graph_button = Button(self.graph_label, text="Draw noisy graph", command=self.draw_noisy_graph, state='disabled')
        self.draw_clean_PSD_button = Button(self.graph_label, text="Draw clean PSD", command=self.draw_clean_PSD_graph, state='disabled')
        self.draw_PSD_graph_button = Button(self.graph_label, text="Draw noisy PSD", command=self.draw_psd_graph, state='disabled')
        self.draw_enhanced_PSD_button = Button(self.graph_label, text="Draw enhanced PSD", command=self.draw_enhanced_PSD_graph, state='disabled')
        self.generate_noise_button = Button(root, text="Add noise and save", command=self.generate_noise, state='disabled')
        self.play_original_sound_button = Button(self.audio_label, text="Play original sound", command=self.play_original_sound, state='disabled')
        self.play_noisy_sound_button = Button(self.audio_label, text="Play noisy sound", command=self.play_noisy_sound, state='disabled')

        """Labels"""
        self.file_loaded_path = Label(root, text="Please select a .wav file using the file menu.")

        """Text Boxes"""
        self.add_snr_textbox = Entry(root)

        """Menu Bars"""
        self.menu_bar = Menu(self.parent)
        self.init_ui()

    @staticmethod
    def play_original_sound():
        controller.play_original_audio()

    @staticmethod
    def play_noisy_sound():
        controller.play_noisy_audio()

    @staticmethod
    def draw_original_graph():
        controller.draw_original_graph()

    @staticmethod
    def draw_noisy_graph():
        controller.draw_noisy_graph()

    def draw_psd_graph(self):
        self.draw_enhanced_PSD_button['state'] = 'normal'
        controller.compute_PSD()

    def draw_clean_PSD_graph(self):
        controller.compute_clean_PSD()

    def generate_noise(self):
        self.play_noisy_sound_button['state'] = 'normal'
        self.draw_noisy_graph_button['state'] = 'normal'
        self.draw_PSD_graph_button['state'] = 'normal'
        var = self.add_snr_textbox.get()
        controller.generate_noise(var)

    def draw_enhanced_PSD_graph(self):
        controller.compute_enhanced_PSD(self)

    def init_ui(self):

        self.parent.title("File dialog")
        self.parent.config(menu=self.menu_bar)

        """Menu"""
        file_menu = Menu(self.menu_bar)
        file_menu.add_command(label="Open", command=self.on_open)
        self.menu_bar.add_cascade(label="File", menu=file_menu)

        """Label Frames"""
        self.graph_label.place(x=425, y=25)
        self.audio_label.place(x=600, y=25)

        """Buttons"""
        self.generate_noise_button.place(x=20, y=40)

        self.draw_graph_button.pack()
        self.draw_noisy_graph_button.pack()
        
        self.draw_clean_PSD_button.pack()
        self.draw_PSD_graph_button.pack()
        self.draw_enhanced_PSD_button.pack()

        self.play_original_sound_button.pack()
        self.play_noisy_sound_button.pack()

        """Labels"""
        self.file_loaded_path.place(x=20, y=20)

        """Text Boxes"""
        self.add_snr_textbox.place(x=180, y=45)


    def on_open(self):

        ftypes = [('Wave Files', '*.wav')]
        dlg = tkFileDialog.Open(self, filetypes=ftypes)
        self.fl = dlg.show()

        if self.fl != '':
            controller.get_wave_header(self.fl)
            self.file_loaded_path['text'] = "File loaded!"
            self.generate_noise_button['state'] = 'normal'
            self.draw_graph_button['state'] = 'normal'
            self.play_original_sound_button['state'] = 'normal'
            self.draw_clean_PSD_button['state'] = 'normal'

    @staticmethod
    def read_file(filename):

        f = open(filename, "r")
        text = f.read()
        return text

def main():
    ex = Main(root)
    root.geometry("800x300")
    root.wm_title("Sound enhancer and analyser")
    root.mainloop()


if __name__ == '__main__':
    main()
